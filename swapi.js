/* eslint-disable camelcase */
const express = require('express')
const app = express()
const port = 3000

const BASE_URL = 'https://swapi.dev/api/'

async function getStarWarsPeople () {
  let total_people = 0
  let people = []
  const promises = []
  // Figure out how many requests we'll need to get all the people
  const response = await fetch(BASE_URL + 'people')
  const data = await response.json()
  total_people = data.count
  const end_page = Math.ceil(total_people / 10)
  for (let i = 1; i <= end_page; ++i) {
    promises.push(fetch(BASE_URL + `/people/?page=${i}`))
  }

  // Request all the people
  const people_data = await Promise.all(promises).then(function (responses) {
    return Promise.all(responses.map(function (response) {
      return response.json()
    }))
  })
  people_data.forEach(element => {
    people.push(element.results)
  })
  people = people.flat()
  return people
}

app.get('/planets', async (req, res) => {
  const people_map = {}
  let total_planets = 0
  let planets = []
  const promises = []

  // The Star Wars api doesn't include the id in the response, so we have to grab it from the url :(
  function getIdFromUrl (url) {
    const url_array = url.split('/')
    const id = url_array.slice(-2)[0]
    return id
  }

  // Figure out how many requests need to be made to get all the planets
  const planet_response = await fetch(BASE_URL + 'planets')
  const data = await planet_response.json()
  total_planets = data.count
  const end_page = Math.ceil(total_planets / 10)

  for (let i = 1; i <= end_page; ++i) {
    promises.push(fetch(BASE_URL + `/planets/?page=${i}`))
  }
  // Planet data requests
  const planets_data = await Promise.all(promises).then(function (responses) {
    return Promise.all(responses.map(function (response) {
      return response.json()
    }))
  })

  planets_data.forEach(element => {
    planets.push(element.results)
  })

  // All the planets in one array
  planets = planets.flat()
  const people = await getStarWarsPeople()
  people.forEach(person => {
    const id = getIdFromUrl(person.url)
    people_map[id] = person.name
  })
  // Resident map populated, now replace urls with names for all the planets
  planets.forEach(planet => {
    const resident_names = []
    planet.residents.forEach(resident_url => {
      const id = getIdFromUrl(resident_url)
      resident_names.push(people_map[id])
    })
    planet.residents = resident_names
  })
  const response = { planets }
  res.setHeader('Content-Type', 'application/json')
  res.send(JSON.stringify(response))
})

app.get('/people', async (req, res) => {
  const people = await getStarWarsPeople()
  if (req.query.sortBy === 'name') {
    people.sort((a, b) => (a.name > b.name) ? 1 : -1)
  } else if (req.query.sortBy === 'height') {
    // Sometimes height/mass is "unknown" and not a number, sort those records to the bottom
    people.sort((a, b) => a.height.replace(',', '').localeCompare(b.height.replace(',', ''), undefined, { numeric: true }))
  } else if (req.query.sortBy === 'mass') {
    people.sort((a, b) => a.mass.replace(',', '').localeCompare(b.mass.replace(',', ''), undefined, { numeric: true }))
  }
  const response = { people }
  res.setHeader('Content-Type', 'application/json')
  res.send(JSON.stringify(response))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
